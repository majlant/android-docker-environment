# Android-Docker-Environment

A docker image for building Android apps. Supports multiple SDK Build Tools.

This Docker image contains the Android SDK and most common packages necessary for building Android apps in a CI tool.

# Docker
```console
$ sudo docker build . -t majlant/android-ci:grr

$ sudo docker images
REPOSITORY           TAG                 IMAGE ID            CREATED             SIZE
majlant/android-ci   grr                 e461bcb799ac        22 hours ago        884MB

$ sudo docker tag e461bcb799ac majlant/android-ci:newtagname
$ sudo docker images
REPOSITORY           TAG                 IMAGE ID            CREATED             SIZE
majlant/android-ci   grr                 e461bcb799ac        22 hours ago        884MB
majlant/android-ci   newtagname             e461bcb799ac        22 hours ago        884MB

$ sudo docker push majlant/android-ci:newtagname

$ sudo docker system prune -a
// WARNING! This will remove:
//        - all stopped containers
//        - all networks not used by at least one container
//        - all images without at least one container associated to them
//        - all build cache
        
sudo docker rmi e461bcb799ac // Remove one or more specific images
```

# Gitlab CI

See [gitlab.ci](.gitlab-ci.yml)